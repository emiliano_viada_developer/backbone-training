<?php 
require 'vendor/autoload.php';
//Instatite Slim
$app = new \Slim\Slim();

//Set all RESPONSES TO JSON
$app->response->headers->set('Content-Type', 'application/json');

//Routes - Controllers
$root = '/backbone-training/api';
//Sign Up
$app->put('/signup', function () use ($app) {
    $user = $app->request->put('user');
    $data = array(
    	'username' => (isset($user['email']))? $user['email'] : 'demo@excedesoft.com',
    	'authenticated' => true
	);
    echo json_encode($data);
});
//Login
$app->get('/login', function () use ($app) {
    $user = $app->request->get('user');
    $data = array(
    	'username' => (isset($user['email']))? $user['email'] : 'demo@excedesoft.com',
    	'authenticated' => true
	);
    echo json_encode($data);
});

//Run it
$app->run();