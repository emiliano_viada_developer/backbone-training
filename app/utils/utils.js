define([
	'app',
	'jquery',
	'underscore',
	'backbone'
	],

function(App, $, _, Backbone) {

	'use strict'

	var serializeObject = function( obj ) {
		var o = {};
		var a = obj.serializeArray();
		$.each(a, function() {
			if (o[this.name] !== undefined) {
				if (!o[this.name].push) {
					o[this.name] = [o[this.name]];
				}
				o[this.name].push(this.value || '');
			} else {
				o[this.name] = this.value || '';
			}
		});

		return o;
	};

	return {
		serializeObject : serializeObject
	};
});