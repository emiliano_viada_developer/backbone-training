define([
	'app',
	'jquery',
	'underscore',
	'backbone'
	],

	function(App, $, _, Backbone) {

	'use strict'

	var DeviceModel = Backbone.Model.extend({
        defaults: {
            name: '',
            serial: 0,
            color: '#666666',
            locationId: 1,
            coords: {
                x:-25.363882,
                y:131.044922
            }
        },

        initialize: function(){

        },

        validate: function( attrs ){
            if ( attrs.name === '' || attrs.serial === '') {
                console.log('Error');
            }
        },

        clear: function(){
            this.detroy();
            this.view.remove();
        }

    });

	return DeviceModel;
});