define([
        'app',
        'jquery',
        'underscore',
        'backbone',
        'models/device',
        'collections/devices'
        ],

        function(App, $, _, Backbone, DeviceModel, DeviceCollection ) {

        'use strict'

        var User = Backbone.Model.extend({

            defaults: {
                id: 1,
                fname: 'John',
                lname: 'Doe',
                email: 'john@doe.com',
                phone: '43434434',
                password: '123'
            },

            url: '/backbone-training/api/index.php/signup',

            // validate: function( attrs, options ) {

            //     var errors = [];

            //     if ( !attrs.fname.length )      errors.push('fname');
            //     if ( !attrs.lname.length )      errors.push('lname');
            //     if ( !attrs.email.length )      errors.push('email');
            //     if ( !attrs.phone.length )      errors.push('phone');
            //     if ( !attrs.password.length )   errors.push('password');

            //     if ( errors.length ) return errors;
            // }


        });


        return User;
});