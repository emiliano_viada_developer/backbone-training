define([
  'app',
  'jquery',
  'underscore',
  'backbone',
  'text!templates/devices.html',
  'footable'

], function( App, $, _, Backbone, DevicesTemplate ) {

  'use strict';

  var DevicesView = Backbone.View.extend({
    template: _.template( DevicesTemplate ),
    events: {

    },

    initialize: function() {
      this.listenTo(this.collection, 'reset', this.render );
    },

    render: function() {
      this.$el.html( this.template( { devices: this.collection.toJSON() } ) );

      this.initFootable();

      return this;
    },

    initFootable: function() {
      $('.devices__table').footable({
        breakpoints: {
            phone: 600
        }
      });
    }

  });

  return DevicesView;

});