define([
	'app',
	'jquery',
	'underscore',
	'backbone',
	'text!templates/signin.html',
	'utils/utils'
	],

function(App, $, _, Backbone, SiginTemplate, Utils) {

	'use strict';

	var SiginView = Backbone.View.extend({
		template: _.template( SiginTemplate ),
		events: {
			'submit' : 'onSubmit'
		},

		initialize: function() {

		},

		render: function() {
			this.$el.html( this.template );

			return this;
		},

		onSubmit: function( e ) {

			var form = Utils.serializeObject( this.$('form') );

			$.ajax({
				url: '/backbone-training/api/index.php/login',
				data: { user: form },
			})
			.done(function(data) {
			})
			.fail(function() {
			})
			.always(function() {
				App.router.navigate('dashboard/overview', { trigger: true });
			});

			e.preventDefault();
		}

	});

	return SiginView;
});
