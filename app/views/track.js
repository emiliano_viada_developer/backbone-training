define([
    'app',
    'jquery',
    'underscore',
    'backbone',
    'text!templates/track.html'
    ],

    function(App, $, _, Backbone, TrackTemplate) {

    'use strict'

    var Track = Backbone.View.extend({
        tagName: 'tr',
        template: _.template( TrackTemplate ),

        events : {
            'click #device__edit' : 'editDevice',
            'click #device__remove' : 'removeDevice'
        },

        initialize : function() {

        },

        render : function() {
            this.$el.append( this.template( this.model.toJSON() ) );

            return this;
        },

        editDevice: function(){

        },

        removeDevice: function(){
            this.model.destroy();
        }

    });


    return Track;
});