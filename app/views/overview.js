define([
    'app',
    'jquery',
    'underscore',
    'backbone',
    'text!templates/overview.html',
    'views/map',
    'views/track',
    'collections/devices',
    'footable'

    ], function( App, $, _, Backbone, OverviewTemplate,  MapView, TrackView ) {

        var OverviewView = Backbone.View.extend({
            template: _.template( OverviewTemplate ),

            initialize: function() {
                this.listenTo(this.collection, 'add', this.renderOne);
                this.listenTo(this.collection, 'reset', this.renderAll);
            },

            render: function( model ) {

                this.$el.html( this.template );

                this.initFootable();

                this.map = new MapView();

                this.$el.prepend( this.map.render().el );

                return this;
            },

            renderOne: function( device ){
                var that = this;
                var view = new TrackView( {model: device} );

                this.$(".overview__table tbody").append( view.render().el );

                setTimeout(function(){
                    that.map.addNewMark( device.toJSON() );
                }, 3000);

            },

            renderAll: function(){
                this.collection.each(this.renderOne, this);
            },


            initFootable: function() {
                $('.device-history__table').footable({
                    breakpoints: {
                        phone: 600
                    }
                });
            }

        });

    return OverviewView;

});