define([
	'app',
	'jquery',
	'underscore',
	'backbone',
	'text!templates/device.html'
	],

	function(App, $, _, Backbone, DeviceTemplate) {

	'use strict';

	var Device = Backbone.View.extend({
		className: 'row',
		template: _.template( DeviceTemplate ),

		events : {
			'click #device__edit' : 'editDevice',
			'click #device__remove' : 'removeDevice'
		},

		initialize : function() {
			this.listenTo(this.model, 'change', this.render);
			this.listenTo(this.model, 'destroy', this.remove);
			this.listenTo(this.model, 'visible', this.toggleVisible);
		},

		render : function() {
			this.$el.empty().append( this.template( this.model.toJSON() ) );

			return this;
		},

		editDevice: function(){

		},

		removeDevice: function(){
			this.model.destroy();
		}

	});


	return Device;
});