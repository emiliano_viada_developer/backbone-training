define([
    'app',
    'jquery',
    'underscore',
    'backbone',
    'text!templates/map.html',
    'utils/gmap'

    ], function( App, $, _, Backbone, MapTemplate, googleMap ) {

        var MapView = Backbone.View.extend({
            template: _.template( MapTemplate ),
            events: {

            },

            initialize: function() {

            },

            render: function() {
                var that = this;
                this.map = {};

                this.$el.html( this.template );

                var canvas = this.$('.map-canvas')[0];

                setTimeout( function(){
                    that.map = new google.maps.Map(canvas, {
                        zoom: 16,
                        position: new google.maps.LatLng(null, null),
                        mapTypeId: google.maps.MapTypeId.ROADMAP
                    });

                    //that.createMap();

                }, 1000);

                return this;
            },

            createMap: function(){
                var pos = new google.maps.LatLng(-31.416935, -64.183886);

                map.setCenter(pos);
            },

            addNewMark: function( device ){
                var myLatlng = new google.maps.LatLng( device.coords.x, device.coords.y );

                var marker = new google.maps.Marker({
                    position: myLatlng,
                    map: this.map,
                    title: 'Hello World!'
                });

                this.map.setCenter(myLatlng);
            }

        });

    return MapView;

});