define([
    'app',
    'jquery',
    'underscore',
    'backbone',
    'text!templates/signup.html',
    'utils/utils'
    ],

function(App, $, _, Backbone, SignupTemplate, Utils) {

    'use strict'

    var SignupView = Backbone.View.extend({

        template: _.template( SignupTemplate ),

        events: {
            "submit" : "onSubmit"
        },

        initialize: function() {
            _.bindAll( this, 'onError' );

            this.listenTo(this.model, 'invalid', this.onError );
        },

        render: function() {
            this.$el.append( this.template );

            return this;
        },

        onSubmit: function( e ) {
            
            var form = Utils.serializeObject( this.$('form') );

            this.model.set({

                fname: form.fname,
                lname: form.lname,
                email: form.email,
                phone: form.phone,
                password: form.password

            }, { validate: true });

            if ( this.model.isValid() ) {
                
                this.model.save();

                App.router.navigate("add-device", { trigger: true });
            }

            e.preventDefault();
        },

        onError: function( model, error ) {

            _.each( error, function( fieldName ) {

                this.setFieldError( fieldName );

            }, this );
        },

        /**
         * UI utilities
         * set and unset "error" status on a field's control-group .
         *
         * It uses internal DOM utilities methods to fetch the DOM nodes from the field name (string)
         */

        setFieldError: function ( fieldName ) {

            var $inputWrapper = this.getFieldWrapper( this.getField(fieldName) );

            $inputWrapper.addClass('has-error');
        },

        resetFieldError: function( fieldName ) {

            var $inputWrapper = this.getFieldWrapper( this.getField(fieldName) );

            $inputWrapper.removeClass('has-error');

        },

        /**
         * DOM Utility
         * supplied the field DOM node by ginving the field's name
         */
        getField: function( fieldName ) {
            return $('input[name='+fieldName+']');
        },

        /**
         * DOM Utility
         * supplies the control-group DOM node that wraps the given field.
         */
        getFieldWrapper: function( $field ) {
            return $field.parents('.form-group');

        }
    });


    return SignupView;
});