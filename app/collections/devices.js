define([
    'app',
    'jquery',
    'underscore',
    'backbone',
    'backboneLocalstorage',
    'models/device'
    ],

    function(App, $, _, Backbone, Store, Device) {

    'use strict'

    var DevicesCollection = Backbone.Collection.extend({
        model: Device,
        localStorage: new Store('tracker-backbone'),

        initialize: function(){
            this.on('add', this.updateSet, this);
        },

        updateSet: function(){
            this.devices = this.models;
        }

    });

    return DevicesCollection;
});