define([
  'app',
  'jquery',
  'underscore',
  'backbone',
  'models/user',
  'views/signin',
  'views/signup'

], function(App, $, _, Backbone, UserModel, SigninView, SignupView) {

  var Home = Backbone.View.extend({
    el: $('#main > .row'),
    events: {
      'click .login__submit' : 'login'
    },

    initialize: function() {
      this.render();
    },

    render: function() {
      this.user = new UserModel();

      var signupView = new SignupView( { model: this.user } );
      var signinView = new SigninView( { model : this.user } );

      this.$el.append( signupView.render().el );
      this.$el.append( signinView.render().el );

      this.$('.csspinner').remove();

      return this;
    }


  });

  return Home;

});