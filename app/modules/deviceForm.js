define([
  'app',
  'jquery',
  'underscore',
  'backbone',
  'text!templates/deviceForm.html',
  'collections/devices',
  'views/device',
  ], 

  function(App, $, _, Backbone, deviceFormTemplate, Devices, DeviceView) {

  var SignUp = Backbone.View.extend({
    el: '#main',
    template: _.template( deviceFormTemplate ),

    events: {
      'click #signup__add' : 'addNewDevice',
      'click #signup__continue' : 'addNewDevice',
      'click #signup__continue' : 'saveForm',
      'blur #device-form__name' : 'updatePlaceholder'

    },

    initialize : function() {
      this.render();
      this.$inputs = this.$('input');
      this.$type = this.$('input:radio');
      this.$name = this.$('#device-form__name');
      this.$serial = this.$('#device-form__serial');
      this.$color = this.$('#device-form__color');
      this.$deviceList = this.$('.signup__devices-list');
      this.$coords = this.$('#device-form__location');
      this.$placeHolder = this.$('.device-form__placeholder');
      this.collection = new Devices();

      this.listenTo(this.collection, 'add', this.addToList );
      this.listenTo(this.collection, 'reset', this.addAll );

      this.collection.fetch();
    },

    render : function() {
      this.$el.html( this.template );

      return this;
    },

    addNewDevice : function(){
      this.collection.create( this.newAttributes() );
      this.$inputs.val('');
      this.$placeHolder.text('');
    },

    addToList: function( device ){
      var view = new DeviceView({ model: device });
      this.$deviceList.append( view.render().el );
    },

    addAll: function(){
      this.collection.each(this.addToList, this);
    },

    // Generate the attributes for a new Device
    newAttributes: function () {
      var coords = this.$coords.val().trim().split(',');

      return {
        type: this.$type.filter(':checked').val(),
        name: this.$name.val().trim(),
        serial: this.$serial.val().trim(),
        color: this.$color.val().trim(),
        coords: {
          x: coords[0],
          y: coords[1]
        }
      };
    },

    updatePlaceholder : function(e) {
      var $el = $(e.currentTarget);

      this.$placeHolder.text( $el.val().trim() );
    },

    saveForm : function(){
      App.router.navigate('dashboard/overview', { trigger: true });
    }

  });

  return SignUp;

});