define([
	'app',
	'jquery',
	'underscore',
	'backbone',
	'text!templates/dashboard.html',
	'views/overview',
	'collections/devices',
	'bootstrap'

], function( App, $, _, Backbone, DashboardTemplate, OverviewView, DevicesCollection ) {

	'use strict';

	var Dashboard = Backbone.View.extend({
		el: '#main',
		template: _.template( DashboardTemplate ),
		events: {

		},

		initialize: function() {
			var that = this;

			this.render();

			// render overview
			this.initOverview();
		},

		render: function() {
			this.$el.empty().html( this.template );

			return this;
		},

		initOverview: function(){
			var col = new DevicesCollection();

			var overview = new OverviewView({ collection : col });
			this.$('#overview').append( overview.render().el );

			col.fetch();
		}

	});

	return Dashboard;

});