// This is the runtime configuration file.  It complements the Gruntfile.js by
// supplementing shared properties.
require.config({

    paths: {

        //Libs & Plugins
        'vendor': '../vendor',
        'almond': '../vendor/bower/almond/almond',
        'underscore': '../vendor/bower/lodash/dist/lodash.underscore',
        'jquery': '../vendor/bower/jquery/dist/jquery',
        'backbone': '../vendor/bower/backbone/backbone',
        'text': '../vendor/bower/text/text',
        'backboneLocalstorage' : '../vendor/bower/backbone.localStorage/backbone.localStorage',
        'serializeJSON' : '../vendor/bower/jquery.serializeJSON/jquery.serializeJSON.min',
        'bootstrap' : '../vendor/bower/bootstrap/dist/js/bootstrap.min',
        'async' : '../vendor/bower/requirejs-plugins/src/async',
        'footable' : '../vendor/bower/jquery.footable/dist/footable.min',

        // Modules
        'utils' : 'utils',
        'templates': 'templates',
        'models' : 'models',
        'collections' : 'collections',
        'views' : 'views'
    },

    shim: {
        "bootstrap": {
            deps: ["jquery"],
            exports: "$.fn.tabs"
        },

        "footable": {
            deps: ["jquery"],
            exports: "$.fn.footable"
        }
    }

});
