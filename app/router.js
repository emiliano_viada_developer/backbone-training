define(function(require, exports, module) {
  "use strict";

  // External dependencies.
  var Backbone = require("backbone");

  // Defining the application router.
  module.exports = Backbone.Router.extend({

    routes : {
      '': 'index',
      'login' : 'login',
      'dashboard/:section' : 'dashboard',
      'add-device' : 'addDevice'
    },

    index : function() {
      var HomeView = require('modules/home');
      var home = new HomeView();
    },

    addDevice : function(){
      var DeviceFormView = require('modules/deviceForm');
      var deviceForm = new DeviceFormView();
    },

    login : function() {
      console.log('login');
    },

    dashboard : function( section ) {
      var DashboardView = require('modules/dashboard');
      var dashboard = new DashboardView();
    }

  });
});